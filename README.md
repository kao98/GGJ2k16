**Global Game Jam 2016** 
  
This is the repository for the Global Game Jam 2016.
It contains a skeleton for a game using the
[Turbulenz Engine](https://github.com/turbulenz/turbulenz_engine/) v1.3.2 (sdk 0.28).  
  
This game is [open-sourced](https://en.wikipedia.org/wiki/Open_source)
and licenced under the
[Creative Commons by-nc-sa 4.0](http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US).  
  
[Global Game Jam](http://globalgamejam.org/)  
[Game Concoillotte](http://gamejam.ofni.asso.fr/)