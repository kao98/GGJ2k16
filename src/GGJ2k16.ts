/// <reference path="jslib-modular/turbulenz.d.ts"  />

/// <reference path="scripts/Application.ts"               />

/*{{ javascript("jslib/draw2d.js") }}*/

/*{{ javascript('scripts/Application.js') }}*/

"use strict";

TurbulenzEngine.onload = function onloadFn(): void {

    var theApplication: Application = new Application();

    TurbulenzEngine.onunload = function onUnloadFn(): void {
        theApplication.shutdown();
        theApplication = null;
    };

    theApplication.onReady = () => { theApplication.run(); };
    theApplication.init();

};
