"use strict";

class Application {

    private intervalId  : any;
    private initialized : boolean;
    private desiredFps  : number    = 60;

    private graphicsDevice  : GraphicsDevice;
    //private draw2d          : Draw2D;
    //private physics2dDevice : Physics2DDevice;
    //private physics3dDevice : PhysicsDevice;
    private inputDevice     : InputDevice;
    private soundDevice     : SoundDevice;

    /**
     * called when the application has been initialized and is ready to be run
     */
    onReady: () => void;

    constructor() {
        this.initialized = false;

        this.graphicsDevice     = TurbulenzEngine.createGraphicsDevice({/*vsyn: true, multisample: 16*/});
        //this.draw2d             = Draw2D.create({graphicsDevice: this.graphicsDevice});
        //this.physics2dDevice    = Physics2DDevice.create();
        //this.physics3dDevice    = TurbulenzEngine.createPhysicsDevice({});
        this.inputDevice        = TurbulenzEngine.createInputDevice({});
        this.soundDevice        = TurbulenzEngine.createSoundDevice({});
    }

    public init(): Application {

        this.intervalId = null;

        this.initialized = true;
        this.onReady();
        return this;
    }

    public run(): Application {

        console.log("Application::run");

        if (!this.initialized) {
            this.init();
        }

        this.intervalId = TurbulenzEngine.setInterval(
            (): void => this._frame()
            , 1000 / this.desiredFps
        );

        return this;
    }

    public shutdown(): Application {

        TurbulenzEngine.clearInterval(this.intervalId);
        this.intervalId = null;

        this.graphicsDevice     = null;
        //this.draw2d             = null;
        //this.physics2dDevice    = null;
        //this.physics3dDevice    = null;
        this.inputDevice        = null;
        this.soundDevice        = null;

        return this;
    }

    /* tslint:disable:no-empty */
    private _frame(): void {

    }
    /* tslint:enable:no-empty */
}
